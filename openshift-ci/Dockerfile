FROM registry.access.redhat.com/ubi8/ubi-minimal

ARG helmVersion=3.6.0
ARG kustomizeVersion=4.1.3
ARG ocVersion=4.7.0

# define environment
ENV helmVersion=${helmVersion}
ENV helmArchive=/tmp/helm.tar.gz
ENV helmDirectory=/opt/helm-${helmVersion}

ENV kustomizeVersion=${kustomizeVersion}
ENV kustomizeArchive=/tmp/kustomize.tar.gz
ENV kustomizeDirectory=/opt/kustomize-${kustomizeVersion}

ENV ocVersion=${ocVersion}
ENV ocDirectory=/opt/oc-${ocVersion}

# install some prereqs
RUN microdnf install curl tar gzip unzip -y && \
    microdnf clean all

# install helm
RUN curl -skL -o ${helmArchive} https://get.helm.sh/helm-v${helmVersion}-linux-amd64.tar.gz && \
    mkdir -p ${helmDirectory} && \
    tar xzf ${helmArchive} -C ${helmDirectory} && \
    ln -sf ${helmDirectory}/linux-amd64/helm /usr/local/bin/helm && \
    chgrp -R 0 ${helmDirectory} && \
    chmod -R g+rwX ${helmDirectory} && \
    rm -rf ${helmArchive}

# install kustomize
RUN curl -skL -o ${kustomizeArchive} https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv${kustomizeVersion}/kustomize_v${kustomizeVersion}_linux_amd64.tar.gz && \
    mkdir -p ${kustomizeDirectory} && \
    tar xzf ${kustomizeArchive} -C ${kustomizeDirectory} && \
    ln -sf ${kustomizeDirectory}/kustomize /usr/local/bin/kustomize && \
    chgrp -R 0 ${kustomizeDirectory} && \
    chmod -R g+rwX ${kustomizeDirectory} && \
    rm -rf ${kustomizeArchive}

# install oc cli
ADD oc-cli/oc-${ocVersion}.tar /usr/local/bin/

USER 1001

CMD ["/bin/bash"]