# pipeline

This repo contains useful tools for building out CI pipelines.

## openshift-ci

Image with basic tools used to deploy apps on OpenShift. 

- Kustomize
- Helm
- OpenShift CLI

## build image

Images are currently being built by quay.io at ericboyer/openshift-ci.

If the default tool versions are insufficent for your needs, the image can be 
rebuilt using the following args:

```
podman build --build-arg helmVersion=3.6.0 \
    --build-arg kustomizeVersion=4.1.3 \
    --build-arg ocVersion=4.7.0 \
    -t openshift-ci -f openshift-ci/Dockerfile
```
